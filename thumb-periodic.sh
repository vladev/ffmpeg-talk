#!/bin/bash

ffmpeg -hide_banner \
       -ss 00:00:40 -i friendly-cat.mp4 \
       -vf 'fps=fps=1/60' \
       -y friendly-cat-%02d.jpg