#!/bin/bash

FRAMES=$(ffprobe -hide_banner -i friendly-cat.mp4 -show_streams -select_streams v | \
		 grep nb_frames | \
	     cut -d = -f 2)

FMOD=$(expr $FRAMES / 9)

ffmpeg -hide_banner \
       -i friendly-cat.mp4 \
       -frames 1 \
       -vf "select=not(mod(n\,$FMOD)),scale=320:180,tile=3x3" \
       -y preview.jpg
