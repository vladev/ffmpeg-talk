#!/bin/bash

ffmpeg -i friendly-10.mp4 \
       -vcodec libvpx \
       -acodec libvorbis \
       -b:v 5000k \
       -y friendly-10-q.webm
