from __future__ import division

import logging
import re
import json
import tempfile
import os
import os.path as p
import errno
from datetime import datetime
from multiprocessing import cpu_count

from shared.media.media import MediaFormat
from shared.transcode.protocol.output import TranscodeOutput, VideoInfo
from shared.transcode.protocol.transcode_pb2 import RECORDING_AUDIO
from transcode.spawn import Processlet, ProcessManager
from transcode.tce.tce import Worker, ProgressCollect
from transcode.util import pretty, check_input_file, parse_dimensions


log = logging.getLogger(__name__)

class AVError(Exception):
    pass


@pretty
class AVInfo(object):
    class Error(AVError):
        pass

    @pretty
    class Video(object):
        def __init__(self, width, height, bit_rate, frame_rate, frames, stream, rotation=0):
            self.width = width
            self.height = height
            self.bit_rate = bit_rate
            self.frame_rate = frame_rate
            self.frames = frames
            self.stream = stream
            self.rotation = rotation

    @pretty
    class Audio(object):
        def __init__(self, bit_rate, sample_rate, stream):
            self.bit_rate = bit_rate
            self.sample_rate = sample_rate
            self.stream = stream

    _INFO_KEYS = ['duration', 'filesize',
                  'v:width', 'v:height', 'v:bit_rate', 'v:frame_rate', 'v:frames', 'v:stream',
                  'v:rotation',
                  'a:bit_rate', 'a:sample_rate', 'a:stream']

    def __init__(self, duration, filesize, audio=None, video=None):
        """
        @type duration float
        @type filesize int
        @type audio AVInfo.Audio
        @type video AVInfo.Video
        """
        self.duration = duration
        self.filesize = filesize
        self.audio = audio
        self.video = video


    @classmethod
    def from_file(cls, input_filename, process_factory=Processlet, tool=None):
        """
        @type input_filename str
        @type process_factory -> Processlet
        @rtype VideoInfo
        @raise VideoInfoError
        """
        data = AVInfo._extract_data(input_filename, process_factory, tool)

        if AVInfo._valid_data(data, 'v:'):
            video = cls.Video(width=data['v:width'],
                              height=data['v:height'],
                              bit_rate=data['v:bit_rate'],
                              frame_rate=data['v:frame_rate'],
                              frames=data['v:frames'],
                              stream=data['v:stream'],
                              rotation=data['v:rotation'])
        else:
            log.warn("Cannot extract video info for %s: %s", input_filename, data)
            video = None

        if AVInfo._valid_data(data, "a:"):
            audio = cls.Audio(bit_rate=data['a:bit_rate'],
                              sample_rate=data['a:sample_rate'],
                              stream=data['a:stream'])
        else:
            log.warn("Cannot extract audio info for %s: %s", input_filename, data)
            audio = None

        try:
            duration = data['duration']
            filesize = data['filesize']
        except KeyError as e:
            log.warn("Could not extract info: %s", repr(e))
            raise AVInfo.Error("Could not extract info from %s", input_filename)

        if not video and not audio:
            raise AVInfo.Error("No video or audio found in %s", input_filename)

        info = cls(duration=duration,
                   filesize=filesize,
                   video=video, audio=audio)

        if not AVInfo._sanity_check(info):
            raise AVInfo.Error("Video did not pass sanity check: %s", input_filename)

        return info

    @staticmethod
    def _sanity_check(info):
        """
        @type info AVInfo
        """
        if 0 > info.duration > 24 * 60 * 60: # 24 hours
            log.warn("Video way too long: %s", info.duration)
            return False

        if 0 > info.filesize > 10 * 1024 * 1024 * 1024: # 10GB
            log.warn("Video way too big: %s", info.filesize)
            return False

        def check_video(info):
            if info.video.width * info.video.height > 10240 * 10240: # 100 megapixels
                log.warn("Bad video resolution: %sx%s. Not considering video.",
                         info.video.width, info.video.height)
                info.video = None
                return

            if info.video.frame_rate <= 0 or info.video.frame_rate > 120:
                log.warn("Bad video framerate: %s. Not considering video.",
                         info.video.frame_rate)
                info.video = None
                return

            if info.video.stream < 0 or info.video.stream > 50:
                log.warn("Bad video stream id: %s. Not considering video.",
                         info.video.stream)
                info.video = None
                return

        def check_audio(info):
            if info.audio.sample_rate <= 0 or info.audio.sample_rate > 256000:
                log.warn("Bad audio sample rate: %s. Not considering audio.",
                         info.audio.sample_rate)
                info.audio = None
                return

            if info.audio.bit_rate <= 0 or info.audio.bit_rate > 10 * 1024 * 1024:  # 10 mbit/s
                log.warn("Bad audio bit rate: %s. Not considering audio.",
                         info.audio.bit_rate)
                info.audio = None
                return

            if info.audio.stream < 0 or info.audio.stream > 50:
                log.warn("Bad audio stream id: %s. Not considering audio.",
                         info.audio.stream)
                info.audio = None
                return

        if info.video:
            check_video(info)

        if info.audio:
            check_audio(info)

        return True

    @staticmethod
    def _extract_data(input_filename, process_factory=Processlet, tool=None):
        if tool == 'mediainfo':
            data = AVInfo._with_mediainfo(input_filename, process_factory)
        elif tool == 'ffprobe':
            data = AVInfo._with_ffprobe(input_filename, process_factory)
        else:
            # Due to a bug in ffmpeg ffprobe doesn't output bit rates for webm,
            # https://trac.ffmpeg.org/ticket/2131
            ffdata = {}
            mediainfodata = {}
            try:
                ffdata = AVInfo._with_ffprobe(input_filename, process_factory)
            except AVInfo.Error as e:
                log.warn("Could not extract video info with for %s ffprobe: %s",
                         input_filename, repr(e))
                mediainfodata = AVInfo._with_mediainfo(input_filename, process_factory)

            if AVInfo._valid_data(ffdata):
                return ffdata

            for k in AVInfo._INFO_KEYS:
                if k not in ffdata:
                    log.warn("Missing ffprobe info: %s", k)
            log.warn("Could not extract all video info with ffprobe %s, using mediainfo: %s",
                     ffdata, input_filename)
            if not mediainfodata:
                mediainfodata = AVInfo._with_mediainfo(input_filename, process_factory)

            data = AVInfo._merge_data(ffdata, mediainfodata)

        if 'v:bit_rate' not in data:
            # Guess bitrate
            log.info("Guessing video bit rate")
            try:
                v_bit_rate = int(data['filesize'] * 8 / data['duration'])
                # We can safely ignore the audio bitrate, since it's comparably small
                if v_bit_rate > 4096: # sanity check - video below 4k would be garbage
                    data['v:bit_rate'] = v_bit_rate
            except KeyError as e:
                log.warn("Could not guess video bit rate: %s", repr(e))

        return data


    @staticmethod
    def _valid_data(data, prefix=''):
        for key in AVInfo._INFO_KEYS:
            if not key.startswith(prefix):
                continue
            if key not in data or data[key] is None:
                log.warn("Missing %s in %s", key, data)
                return False

        return True

    @staticmethod
    def _merge_data(*data_sources):
        d = {}
        for key in AVInfo._INFO_KEYS:
            for ds in data_sources:
                if key in ds:
                    d[key] = ds[key]
                    break

        return d

    @staticmethod
    def _with_ffprobe(input_filename, process_factory):
        p = process_factory(['ffprobe', '-hide_banner', '-loglevel', 'quiet',
                             '-print_format', 'json', '-show_streams', '-show_format',
                             input_filename],
                            timeout=10, ignore_stderr=False)
        p.join()
        if p.returncode != 0:
            raise AVInfo.Error("ffprobe exited with {} for {}".format(
                p.returncode, input_filename))

        try:
            raw = json.loads(''.join(p.stdout))
        except ValueError as e:
            raise AVInfo.Error("Error parsing ffprobe output for {}: {}".format(
                input_filename, repr(e)))

        data = {}
        try:
            format_ = raw['format']
            streams = raw['streams']
            # 'format' and 'streams' will always be present, they are internal to ffprobe

            if 'duration' in format_:
                data['duration'] = float(format_['duration'])
            if 'size' in format_:
                data['filesize'] = int(format_['size'])

            # Take only the first video and first audio stream
            video_extracted = False
            audio_extracted = False
            for stream in streams:
                if stream['codec_type'] == 'video' and not video_extracted:
                    if 'avg_frame_rate' in stream:
                        num, den = map(float, stream['avg_frame_rate'].split('/'))

                    if (not num or not den) and 'r_frame_rate' in stream:
                        num, den = map(float, stream['r_frame_rate'].split('/'))

                    if num and den:
                        data['v:frame_rate'] = round(num / den, 2)

                    if 'width' in stream:
                        data['v:width'] = int(stream['width'])
                    if 'height' in stream:
                        data['v:height'] = int(stream['height'])
                    if 'bit_rate' in stream:
                        data['v:bit_rate'] = int(stream['bit_rate'])

                    if 'tags' in stream and 'rotate' in stream['tags']:
                        data['v:rotation'] = int(stream['tags']['rotate'])
                    else:
                        data['v:rotation'] = 0

                    if 'nb_frames' in stream:
                        data['v:frames'] = int(stream['nb_frames'])
                    elif 'duration' in data and 'v:frame_rate' in data:
                        data['v:frames'] = int(data['duration'] * data['v:frame_rate'])

                    if 'index' in stream:
                        data['v:stream'] = int(stream['index'])

                    video_extracted = True

                elif stream['codec_type'] == 'audio' and not audio_extracted:
                    if 'bit_rate' in stream:
                        data['a:bit_rate'] = int(stream['bit_rate'])
                    if 'sample_rate' in stream:
                        data['a:sample_rate'] = int(stream['sample_rate'])
                    if 'index' in stream:
                        data['a:stream'] = int(stream['index'])
                    audio_extracted = True

        except (TypeError, ValueError) as e:
            raise AVInfo.Error("Error collecting info for {}: {}".format(
                input_filename, repr(e)))

        return data

    @staticmethod
    def _with_mediainfo(input_filename, process_factory):
        mediainfo_template = "General;type=general,duration=%Duration%,filesize=%FileSize%\\n\n" \
                             "Video;type=video,width=%Width%,height=%Height%," \
                                "bit_rate=%BitRate%,frame_rate=%FrameRate%," \
                                "frame_count=%FrameCount%,stream_id=%ID%," \
                                "rotation=%Rotation%\\n\n" \
                             "Audio;type=audio,bit_rate=%BitRate%,sampling_rate=%SamplingRate%," \
                                "stream_id=%ID%\\n\n"

        with tempfile.NamedTemporaryFile(mode='wb') as tf:
            tf.write(mediainfo_template)
            tf.flush()
            p = process_factory(['mediainfo',
                                 '--Inform=file://{}'.format(tf.name),
                                 input_filename])
            p.join()

        if p.returncode != 0:
            raise AVInfo.Error("mediainfo for {} exited with code {}".format(
                input_filename, p.returncode))

        # Mediainfo output looks like:
        # type=general,duration=1005,filesize=120722
        # type=video,width=720,height=528,bit_rate=,frame_rate=25.000,frame_count=25,stream_id=1
        # type=video,width=720,height=528,bit_rate=,frame_rate=25.000,frame_count=25,stream_id=2
        # type=audio,bit_rate=112000,sampling_rate=44100,stream_id=3
        # type=audio,bit_rate=112000,sampling_rate=44100,stream_id=4

        raw_data = []
        for line in p.stdout:
            line = line.strip()
            if not line:
                continue
            kvs = line.split(',')
            line_values = {}
            for kv in kvs:
                k, v = kv.split('=')
                if k and v:
                    line_values[k] = v

            raw_data.append(line_values)

        data = {}
        # Take only the first video and first audio stream
        video_extracted = False
        audio_extracted = False
        try:
            for stream in raw_data:
                if stream['type'] == 'video' and not video_extracted:
                    if 'width' in stream:
                        data['v:width'] = int(stream['width'])
                    if 'height' in stream:
                        data['v:height'] = int(stream['height'])
                    if 'bit_rate' in stream:
                        data['v:bit_rate'] = int(stream['bit_rate'])
                    if 'frame_rate' in stream:
                        data['v:frame_rate'] = float(stream['frame_rate'])

                    if 'frame_count' in stream:
                        data['v:frames'] = int(stream['frame_count'])

                    if 'rotation' in stream:
                        data['v:rotation'] = int(float(stream['rotation']))
                        # int(float) because it looks like 180.000
                    else:
                        data['v:rotation'] = 0

                    if 'stream_id' in stream:
                        try:
                            # mediainfo count is 1-based
                            data['v:stream'] = int(stream['stream_id']) - 1
                        except ValueError:
                            log.warn("Invalid stream_id from mediainfo: %s", stream['stream_id'])
                    video_extracted = True

                elif stream['type'] == 'audio' and not audio_extracted:
                    if 'bit_rate' in stream:
                        data['a:bit_rate'] = int(stream['bit_rate'])
                    if 'sampling_rate' in stream:
                        data['a:sample_rate'] = int(stream['sampling_rate'])

                    if 'stream_id' in stream:
                        try:
                            # mediainfo count is 1-based
                            data['a:stream'] = int(stream['stream_id']) - 1
                        except ValueError:
                            log.warn("Invalid stream_id from mediainfo: %s", stream['stream_id'])

                    audio_extracted = True

                elif stream['type'] == 'general':
                    if 'duration' in stream:
                        data['duration'] = float(stream['duration']) / 1000
                    if 'filesize' in stream:
                        data['filesize'] = int(stream['filesize'])

        except (ValueError, TypeError) as e:
            raise AVInfo.Error("Error collecting with mediainfo for {}: {}".format(
                input_filename, repr(e)))

        return data


@pretty
class Progress(object):
    def __init__(self, seconds, frame, q=None, fps=None, bit_rate=None, size=None):
        self.seconds = seconds
        self.frame = frame
        self.q = q
        self.fps = fps
        self.bit_rate = bit_rate
        self.size = size


class FFmpegProgressParser(object):
    RE = re.compile('(?P<key>\w+)=\s*(?P<value>[^\s]+)')

    def parse(self, line):
        match = self.RE.findall(line)
        if match:
            values = dict(match)
            if 'time' not in values:
                return None
            frame = int(values.get('frame', 0))
            try:
                t = datetime.strptime(values['time'], '%H:%M:%S.%f')
            except ValueError as e:
                log.warn("Error parsing ffmpeg output: %s, %s", line, repr(e))
                return None

            seconds = t.hour * 3600 + t.minute * 60 + t.second + t.microsecond / 1000000.
            return Progress(seconds=seconds,
                            frame=frame,
                            q=values['q'] if 'q' in values else None,
                            fps=float(values['fps']) if 'fps' in values else None,
                            bit_rate=values.get('bitrate'),
                            size=values.get('size'))


class Resize(object):
    def __init__(self, original_width, original_height):
        self.original_width = original_width
        self.original_height = original_height

    def fit(self, width, height, div_by=8):
        width_scale = width / self.original_width
        height_scale = height / self.original_height

        if width_scale > 1 and height_scale > 1:
            # No up-scaling
            new_width = self.original_width
            new_height = self.original_height

        elif width_scale > height_scale:
            # only scaling the width, the height is capped
            new_width = self.original_width * height_scale
            new_height = height
        else:
            new_width = width
            new_height = self.original_height * width_scale

        return (Resize.round_to_nearest_div(new_width, div_by),
                Resize.round_to_nearest_div(new_height, div_by))

    @staticmethod
    def round_to_nearest_div(num, div_by):
        return int((num + div_by / 2) / div_by) * div_by


class Bitrates(object):
    SUPPORTED_AUDIO_BITRATES = [16, 32, 48, 64, 96, 128, 192, 256, 320]

    R1080P = 1920 * 1080
    R720P = 1280 * 720
    RVGA = 640 * 480
    RQVGA = 320 * 240

    TOTAL_BITRATES = {
        # resolution: (total bitrate, audio bitrate)
        R1080P: (8192, 256),
        R720P: (5120, 192),
        RVGA: (2048, 128),
        RQVGA: (512, 96),
    }

    def __init__(self, width, height, video_bit_rate, audio_bit_rate=None):
        self.width = width
        self.height = height
        self.video_bit_rate = video_bit_rate
        self.audio_bit_rate = audio_bit_rate

    def normal(self):
        return self._bitrate(self.TOTAL_BITRATES)

    def mobile(self):
        no_1080p = self.TOTAL_BITRATES.copy()
        del no_1080p[self.R1080P]

        return self._bitrate(no_1080p)

    def _bitrate(self, bitrates):
        quality = Bitrates.round_to(self.width * self.height, bitrates.keys())
        total, audio = bitrates[quality]

        if self.audio_bit_rate:
            audio = Bitrates.round_to(min(audio, self.audio_bit_rate),
                                      self.SUPPORTED_AUDIO_BITRATES)
        else:
            audio = 0
        video = total - audio

        return int(video), int(audio)
        # return int(min(video, self.video_bit_rate * 1.5)), int(audio)

    @staticmethod
    def round_to(value, steps):
        """
        @type value int
        @type steps iterable
        """
        steps = list(steps)
        steps.sort(reverse=True)
        for step in steps:
            if value >= step:
                return step

        return steps[-1]


def keyframes(frame_rate):
    min_ = int(1 * frame_rate)
    max_ = int(3 * frame_rate)
    return min_, max_


def video_filters(av_info, max_width, max_height):
    """
    Use '-filter:v' option before passing the return value to ffmpeg.

    @type av_info AVInfo
    """
    rotation = av_info.video.rotation

    filters = []

    if rotation == 90:
        filters.append('transpose=1')
    elif rotation == 270:
        filters.append('transpose=2')
    elif rotation == 180:
        filters.extend(['hflip','vflip'])

    if rotation in (90, 270):
        # swap width and height
        resize = Resize(av_info.video.height, av_info.video.width)
    else:
        resize = Resize(av_info.video.width, av_info.video.height)

    width, height = resize.fit(max_width, max_height)

    # filter order matters
    filters.append('scale={}:{}'.format(width, height))

    return ','.join(filters)

class ThumbnailExtract(object):
    class Error(AVError):
        pass


    def thumbnail(self, input_filename, av_info, thumbnail_width, thumbnail_height,
                  thumbnail_filename, process_factory=Processlet):
        """
        @type input_filename str
        @type av_info AVInfo
        @type thumbnail_filename str
        @type process_factory -> Processlet
        @type async bool
        """
        w, h = ThumbnailExtract._compute_size(av_info.video.width, av_info.video.height,
                                              thumbnail_width, thumbnail_height)

        if av_info.duration > 120:
            # if movie longer than 2 mins - jump to minute 1.5 and take the frame
            start = 90
            look_for = 1
            thumb_frame = 0
        else:
            # otherwise - take the middle frame
            start = 0
            look_for = int(av_info.duration)
            thumb_frame = int(av_info.duration / 2 * av_info.video.frame_rate)

        ffargs = ['ffmpeg',
                  '-ss', start,
                  '-i', input_filename,
                  '-t', look_for,
                  '-y',
                  '-hide_banner',
                  '-frames:v', '1',
                  '-vsync', 'vfr',
                  '-an',
                  '-filter:v', r"select='gt(n\,{})',scale={}:{}".format(thumb_frame, w, h),
                  thumbnail_filename]
        p = process_factory(ffargs, redirect_stderr=True, timeout=60)
        p.join()

        if p.returncode != 0:
            raise ThumbnailExtract.Error(
                "Subprocess exited with {} code.".format(p.returncode))

    @staticmethod
    def _compute_size(original_width, original_height, new_width, new_height):
        width_mul = new_width / original_width
        height_mul = new_height / original_height
        mul = min(width_mul, height_mul)

        w = original_width * mul
        h = original_height * mul
        w = int(round(w))
        h = int(round(h))
        return w, h


def to_kb(b):
    return round(b / 1000, 0)

class AVTranscoder(object):
    class Error(AVError):
        pass

    CPU_COUNT = cpu_count()

    # Map bitrate to vorbis quality values
    VORBIS_BITRATE_Q_MAP = {
        16: -2,
        32: -2,
        48: -1,
        64: 0,
        96: 2,
        128: 4,
        192: 6,
        256: 8,
        320: 9
    }

    def __init__(self):
        self._progress_parse = FFmpegProgressParser()

    def transcode_single_pass(self, input_filename, av_info,
                              output_filename_prefix,
                              max_width=1920, max_height=1080,
                              mobile_max_width=1280, mobile_max_height=720,
                              progress_cb=None, process_factory=Processlet,
                              timeout=None, max_threads=3):

        if av_info.video:
            args, outputs = self._video_args(input_filename=input_filename, av_info=av_info,
                                             output_filename_prefix=output_filename_prefix,
                                             max_width=max_width, max_height=max_height,
                                             mobile_max_width=mobile_max_width,
                                             mobile_max_height=mobile_max_height,
                                             max_threads=max_threads)
        else:
            args, outputs = self._audio_args(input_filename=input_filename, av_info=av_info,
                                             output_filename_prefix=output_filename_prefix,
                                             max_threads=max_threads)


        log.info("Doing single pass %s, outputing to: %s", ' '.join(map(str, args)), outputs)
        p = process_factory(args, redirect_stderr=True,
                            timeout=timeout, stdout_timeout=int(timeout/5) if timeout else None)

        progress_collect = ProgressCollect(['single_pass'])
        percent = 0
        for line in p.stdout:
            progress = self._progress_parse.parse(line)
            if not progress:
                continue
            progress_collect.progress('single_pass',
                                      total=av_info.duration,
                                      current=progress.seconds)
            if percent < progress_collect.overall_percent and callable(progress_cb):
                percent = progress_collect.overall_percent
                progress_cb(percent)

        p.join()
        if p.returncode != 0:
            raise AVTranscoder.Error("Subprocess exited with {} code.".format(p.returncode))

        return outputs

    def _audio_args(self, input_filename, av_info, output_filename_prefix, max_threads):
        outputs = []

        mp3out = output_filename_prefix + '.mp3'
        oggout = output_filename_prefix + '.ogg'

        ffargs = [
            'ffmpeg',
            '-i', input_filename,
            '-y',
            '-hide_banner']

        threads = min(max_threads, self.CPU_COUNT)

        # mp3 output
        mp3args = ['-codec:a', 'libmp3lame',
                   '-q:a', '2',
                   '-map', '0:{}'.format(av_info.audio.stream),
                   '-ac', 2,  # stereo
                   '-ar', 22050,
                   '-threads', threads,
                   '-f', 'mp3',
                   mp3out,
        ]
        ffargs.extend(mp3args)
        outputs.append(mp3out)

        # ogg output
        oggargs = ['-codec:a', 'libvorbis',
                   '-q:a', '6',
                   '-map', '0:{}'.format(av_info.audio.stream),
                   '-ac', 2,  # stereo
                   '-ar', 22050,
                   '-threads', threads,
                   '-f', 'ogg',
                   oggout,
        ]
        ffargs.extend(oggargs)
        outputs.append(oggout)

        return ffargs, outputs

    def _video_args(self, input_filename, av_info, output_filename_prefix,
               max_width, max_height, mobile_max_width, mobile_max_height,
               max_threads):
        """
        @type av_info AVInfo
        """
        if not av_info.video:
            raise AVTranscoder.Error("Video {} is actually missing a video stream".format(
                input_filename))

        keyframe_min, keyframe_max = keyframes(av_info.video.frame_rate)

        # resize = Resize(av_info.video.width, av_info.video.height)
        # width, height = resize.fit(max_width, max_height)
        # mobile_width, mobile_height = resize.fit(mobile_max_width, mobile_max_height)

        br = Bitrates(av_info.video.width, av_info.video.height, to_kb(av_info.video.bit_rate),
                      to_kb(av_info.audio.bit_rate) if av_info.audio else None)
        video_br, audio_br = br.normal()
        video_mobile_br, audio_mobile_br = br.mobile()

        outputs = []
        mp4out = output_filename_prefix + '.mp4'
        webmout = output_filename_prefix + '.webm'
        mp4mobileout = output_filename_prefix + '.mobile.mp4'
        webmmobileout = output_filename_prefix + '.mobile.webm'

        # Threads:
        # 0 means auto
        # 3 or more starts causing loss of time waiting for syscalls
        # Going higher than 5 makes things worse.
        threads = min(max_threads, self.CPU_COUNT)

        x264args = []
        vp8args = []
        x264mobileargs = []
        vp8mobileargs = []

        # x264 video...
        x264args.extend([
            '-codec:v', 'libx264',
            '-filter:v', video_filters(av_info, max_width, max_height),
            '-metadata:s:v', 'rotate=',  # remove rotate metadata
            '-profile:v', 'main',
            '-level', '3.1',
            '-movflags', 'faststart',
            '-crf', '20',  # 23 - default, less is better
            '-b:v', '{}ki'.format(video_br),
            '-g', keyframe_max,
            '-keyint_min', keyframe_min,
            '-map', '0:{}'.format(av_info.video.stream),
            '-r', av_info.video.frame_rate, # Preserve the original fps
            '-pix_fmt', 'yuv420p', # player compatibility
            '-preset', 'fast',
        ])
        if av_info.audio:
            # ...with aac audio...
            x264args.extend([
                '-codec:a', 'libfdk_aac',
                # '-q:a', '100',  # 10-500
                '-b:a', '{}ki'.format(audio_br),
                '-map', '0:{}'.format(av_info.audio.stream),
                '-ac', 2, # stereo
            ])
        else:
            # no audio
            x264args.extend(['-an'])
        # ...in a mp4 container.
        x264args.extend([
            '-f', 'mp4',
            '-threads', threads,
            mp4out,
        ])
        outputs.append(mp4out)

        # vp8 video...
        vp8args.extend([
            '-codec:v', 'libvpx',
            '-filter:v', video_filters(av_info, max_width, max_height),
            '-metadata:s:v', 'rotate=',  # remove rotate metadata
            '-crf', '15',  # 4-63 - lower is better quality
            '-b:v', '{}ki'.format(video_br),
            '-g', keyframe_max,
            '-keyint_min', keyframe_min,
            '-r', av_info.video.frame_rate, # Preserve the original fps
            '-deadline', 'good',
            '-cpu-used', 2,
        ])
        if av_info.audio:
            # ...with vorbis audio...
            vp8args.extend([
                '-codec:a', 'libvorbis',
                '-q:a', self.VORBIS_BITRATE_Q_MAP[audio_br],
                # '-b:a', '{}ki'.format(audio_br),
                '-ac', 2, # stereo
                ])
        else:
            # no audio
            vp8args.extend(['-an'])
        # ...in a webm container.
        vp8args.extend([
            '-f', 'webm',
            '-threads', threads,
            webmout,
            ])
        outputs.append(webmout)

        if (video_br > video_mobile_br or
                    av_info.video.width > mobile_max_width or
                    av_info.video.height > mobile_max_height):
            # we need to produce something less demanding for mobile
            # x264 mobile video...
            x264mobileargs.extend([
                '-codec:v', 'libx264',
                '-filter:v', video_filters(av_info, mobile_max_width, mobile_max_height),
                '-metadata:s:v', 'rotate=',  # remove rotate metadata
                '-profile:v', 'main',
                '-level', '3.1',
                '-movflags', 'faststart',
                '-crf', '20',  # 23 - default, less is better
                '-b:v', '{}ki'.format(video_mobile_br),
                '-g', keyframe_max,
                '-keyint_min', keyframe_min,
                '-map', '0:{}'.format(av_info.video.stream),
                '-r', av_info.video.frame_rate, # Preserve the original fps
                '-pix_fmt', 'yuv420p', # player compatibility
                '-preset', 'fast',
            ])
            if av_info.audio:
                # ...with aac audio...
                x264mobileargs.extend([
                    '-codec:a', 'libfdk_aac',
                    # '-q:a', '100',  # 10-500
                    '-b:a', '{}ki'.format(audio_mobile_br),
                    '-map', '0:{}'.format(av_info.audio.stream),
                    '-ac', 2, # stereo
                    ])
            else:
                # no audio
                x264mobileargs.extend(['-an'])
            # ...in a mp4 container.
            x264mobileargs.extend([
                '-f', 'mp4',
                '-threads', threads,
                mp4mobileout,
                ])
            outputs.append(mp4mobileout)

            # vp8 mobile video...
            vp8mobileargs.extend([
                '-codec:v', 'libvpx',
                '-filter:v', video_filters(av_info, mobile_max_width, mobile_max_height),
                '-metadata:s:v', 'rotate=',  # remove rotate metadata
                '-crf', '15',  # 4-63 - lower is better quality
                '-b:v', '{}ki'.format(video_mobile_br),
                '-g', keyframe_max,
                '-keyint_min', keyframe_min,
                '-r', av_info.video.frame_rate, # Preserve the original fps
                '-deadline', 'good',
                '-cpu-used', 2,
            ])
            if av_info.audio:
                # ...with vorbis audio...
                vp8mobileargs.extend([
                    '-codec:a', 'libvorbis',
                    '-q:a', self.VORBIS_BITRATE_Q_MAP[audio_mobile_br],
                    # '-b:a', '{}ki'.format(audio_mobile_br),
                    '-ac', 2, # stereo
                    ])
            else:
                # no audio
                vp8mobileargs.extend(['-an'])
            # ...in a webm container.
            vp8mobileargs.extend([
                '-f', 'webm',
                '-threads', threads,
                webmmobileout,
                ])
            outputs.append(webmmobileout)

        ffargs = [
            'ffmpeg',
            '-i', input_filename,
            '-y',
            '-hide_banner']
        ffargs.extend(x264args)
        if x264mobileargs:
            ffargs.extend(x264mobileargs)

        ffargs.extend(vp8args)
        if vp8mobileargs:
            ffargs.extend(vp8mobileargs)

        return ffargs, outputs


class AV(Worker):
    def __init__(self, config):
        self.media_dir = config['media_dir']
        self.default_timeout = int(config.get('default_timeout', 3600))
        self.timeout_multiplier = float(config.get('timeout_multiplier', 4))
        self.timeout_offset = int(config.get('timeout_offset', 60))
        self.output_dir = config.get('output_dir', 'converted')

        try:
            os.mkdir(p.join(self.media_dir, self.output_dir))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        self.thumbnail_size = parse_dimensions(config.get('thumbnail_size', '85x85'))
        self.max_resolution = parse_dimensions(config.get('max_resolution', '1920x1080'))

        self.avt = AVTranscoder()
        self.te = ThumbnailExtract()

    def process(self, task, progress_cb):
        filepath = task.current_stage.input_files[0].filepath
        is_recording = task.context.action == RECORDING_AUDIO
        return self.process_file(filepath, progress_cb, is_recording)

    def process_file(self, filepath, progress_cb, is_recording=False):
        # path: = Examples =
        # path: media_dir=/home/prod_fuze/media_store
        # path: filepath=upload/video.avi
        # path: output_dir=converted

        filepath = p.join(self.media_dir, filepath)
        # path: filepath=/home/prod_fuze/media_store/upload/video.avi

        check_input_file(filepath)

        relative_base = p.join(self.output_dir, p.basename(filepath))
        # path: relative_base=converted/video.avi
        full_base = p.join(self.media_dir, relative_base)
        # path: full_base=/home/prod_fuze/media_store/converted/video.avi
        out_tmp_prefix = full_base + '.tmp'
        # path: out_tmp_prefix=/home/prod_fuze/media_store/converted/video.avi.tmp
        thumbout = out_tmp_prefix + '.png'
        # path: thumbout=/home/prod_fuze/media_store/converted/video.avi.tmp.png

        with ProcessManager() as process_manager:
            av_info = AVInfo.from_file(input_filename=filepath,
                                       process_factory=process_manager.spawn)

            timeout = self._calculate_timeout(av_info=av_info)

            outputs = self.avt.transcode_single_pass(input_filename=filepath, av_info=av_info,
                                                     output_filename_prefix=out_tmp_prefix,
                                                     max_width=self.max_resolution[0],
                                                     max_height=self.max_resolution[1],
                                                     progress_cb=progress_cb,
                                                     process_factory=process_manager.spawn,
                                                     timeout=timeout)
            # 'outputs' is a list containing some of:
            # path: /home/prod_fuze/media_store/converted/video.avi.tmp.mp4
            # path: /home/prod_fuze/media_store/converted/video.avi.tmp.webm
            # path: /home/prod_fuze/media_store/converted/video.avi.tmp.mobile.mp4
            # path: /home/prod_fuze/media_store/converted/video.avi.tmp.mobile.webm

            # Extracting from some of the outputs is much more reliable,
            # since they are well behaving files, rather than the crap
            # we get as input.
            video_output = None
            for out in outputs:
                if out.endswith('.mp4') or out.endswith('.webm'):
                    video_output = out
                    break
            if video_output:
                firstinfo = AVInfo.from_file(video_output)
                self.te.thumbnail(input_filename=video_output,
                                  av_info=firstinfo,
                                  thumbnail_width=self.thumbnail_size[0],
                                  thumbnail_height=self.thumbnail_size[1],
                                  thumbnail_filename=thumbout,
                                  process_factory=process_manager.spawn)
                outputs.append(thumbout)

        out = TranscodeOutput(self.media_dir)

        if not is_recording:
            audio_format = MediaFormat.AUDIO
        else:
            audio_format = MediaFormat.AUDIO_RECORD_STREAM

        for o in outputs:
            if o.endswith('.png'):
                # path: thumbout=/home/prod_fuze/media_store/converted/video.avi.tmp.png
                # path: renamed to /home/prod_fuze/media_store/converted/video.avi.png
                move(o, full_base + '.png')
                out.add_output_file(relative_base + '.png', MediaFormat.THUMBNAIL)
            elif o.endswith('.mp3'):
                move(o, full_base + '.mp3')
                out.add_output_file(relative_base + '.mp3', audio_format)
            elif o.endswith('.ogg'):
                move(o, full_base + '.ogg')
                out.add_output_file(relative_base + '.ogg', MediaFormat.AUDIO_OGG)
            else:
                info = self._video_info_output(o)
                if o.endswith('.mobile.mp4'):
                    move(o, full_base + '.mobile.mp4')
                    out.add_output_file(relative_base + '.mobile.mp4', MediaFormat.IPHONE_MOVIE,
                                        video_info=info)
                elif o.endswith('.mobile.webm'):
                    move(o, full_base + '.mobile.webm')
                    out.add_output_file(relative_base + '.mobile.webm', MediaFormat.WEBM_MOBILE_MOVIE,
                                        video_info=info)
                elif o.endswith('.mp4'):
                    move(o, full_base + '.mp4')
                    out.add_output_file(relative_base + '.mp4', MediaFormat.FLASH_MOVIE,
                                        video_info=info)
                elif o.endswith('.webm'):
                    move(o, full_base + '.webm')
                    out.add_output_file(relative_base + '.webm', MediaFormat.WEBM_MOVIE,
                                        video_info=info)
                else:
                    log.warn("Unsupported output: %s", o)
        return out

    def _calculate_timeout(self, av_info):
        if av_info.video:
            megapixels = av_info.video.width * av_info.video.height / 1000000
            timeout = (int(self.timeout_multiplier * av_info.video.frames * megapixels) +
                       self.timeout_offset)
        elif av_info.duration:
            timeout = int(self.timeout_multiplier * av_info.duration / 5) + self.timeout_offset
        else:
            timeout = self.default_timeout

        return timeout

    def _video_info_output(self, filepath):
        try:
            info = AVInfo.from_file(filepath)
            return VideoInfo(duration_ms=info.duration * 1000,
                             framerate=info.video.frame_rate,
                             bitrate=info.video.bit_rate + (info.audio.bit_rate if info.audio else 0))
        except Exception:
            log.error("Error collecting output file '%s' info", filepath, exc_info=1)
            return VideoInfo(0, 0, 0)

def move(src, dst):
    # log.debug("Moving %s -> %s", src, dst)
    os.rename(src, dst)