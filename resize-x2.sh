#!/bin/bash

ffmpeg -i friendly-10.mp4 \
       -filter:v 'scale=800x448' \
       -acodec copy \
       -y friendly-10-800x448.mp4 \
       -filter:v 'scale=1280x720' \
       -acodec copy \
       -y friendly-10-1280x720.mp4

