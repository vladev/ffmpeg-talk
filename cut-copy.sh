#!/bin/bash

ffmpeg -ss 22 \
       -i friendly-cat.mp4 \
       -acodec copy \
       -vcodec copy \
       -t 10 \
       -y friendly-10-copy.mp4
