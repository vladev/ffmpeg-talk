#!/bin/bash

ffmpeg -ss 00:00:40 -i friendly-cat.mp4 \
       -vf 'select=eq(n\,0),scale=800x450' \ // HL
       -frames:v 1 \
       -y \
       friendly-cat.jpg
