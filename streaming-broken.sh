#!/bin/bash

cat friendly-10.mp4 | ffmpeg \
       -hide_banner \
       -i - \
       -vcodec libvpx \
       -acodec libvorbis \
       -b:v 5000k \
       -f webm \
       - > fs.mp4
