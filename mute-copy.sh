#!/bin/bash

ffmpeg -i friendly-10.mp4 \
       -an \
       -vcodec copy \
       -y friendly-muted-10-copy.mp4
