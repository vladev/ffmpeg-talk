#!/bin/bash

ffmpeg -i friendly-10.mp4 \
       -vcodec libvpx \
       -acodec libvorbis \
       -y friendly-10.webm
