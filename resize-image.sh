#!/bin/bash

ffmpeg -ss 00:00:40 -i friendly-cat.mp4 \
       -vf 'select=eq(n\,0)' \
       -frames:v 1 \
       -y \
       friendly-cat-hd.png

ffmpeg -i friendly-cat-hd.png \
       -filter:v 'scale=1024x600' \ // HL
       -y friendly-cat-sd.jpg
