#!/bin/bash

ffmpeg -i friendly-10.mp4 \
       -filter:v 'scale=800x448' \ // HL
       -acodec copy \
       -y friendly-10-800x448.mp4
